const webtorrent = require("webtorrent"),
	{ ipcRenderer } = window.require("electron"),
	client = new webtorrent();

client.on('error', function (err) {
	console.err(err);
});

function jsonifyTorrent(torrent) {
	return JSON.stringify({
		files: torrent.files.map(file => ({
			name: file.name,
			path: file.path,
			length: file.length
		})),
		magnetURI: decodeURIComponent(torrent.magnetURI),
		name: torrent.name,
		numPeers: torrent.numPeers,
		progress: torrent.progress,
		downloadSpeed: torrent.downloadSpeed,
		downloaded: torrent.downloaded,
		length: torrent.length,
		path: torrent.path,
		timeRemaining: torrent.timeRemaining,
		destroyed: torrent.destroyed,
		id: torrent.magnetURI.slice(20, 60)
	});
}

function addTorrent(magnet, options) {
	return new Promise(resolve => {
		const alreadyExists = client.torrents.find(torrent => torrent.magnetURI.slice(20, 60) === magnet.slice(20, 60));
		if (alreadyExists) return resolve(jsonifyTorrent(alreadyExists));
		let torrentAddedSuccessfully = false;
		client.add(magnet, options, torrent => {
			let timeSinceLastUpdate = Date.now();
			torrent.on("download", () => {
				if (timeSinceLastUpdate > Date.now() - 500) return;
				timeSinceLastUpdate = Date.now();
				ipcRenderer.send("sendToMainWindow", "torrentInfoUpdate", jsonifyTorrent(torrent));
			});
			torrent.on("done", () => {
				ipcRenderer.send("sendToMainWindow", "torrentFinished", jsonifyTorrent(torrent));
			});
			resolve(jsonifyTorrent(torrent));
			torrentAddedSuccessfully = true;
		});
		setTimeout(() => {
			if (!torrentAddedSuccessfully) {
				const alreadyExists = client.torrents.find(torrent => torrent.magnetURI.slice(20, 60) === magnet.slice(20, 60));
				if (alreadyExists)
					alreadyExists.destroy();
				resolve(addTorrent(magnet, options));
			}
		}, 60000);
	});
}

function getAllTorrents() {
	return "[" + client.torrents.map(jsonifyTorrent).join(",") + "]";
}

function getTorrentsLength() {
	return client.torrents.length;
}

function createServer(torrent) {
	const found = client.torrents.find(ele => ele.magnetURI.slice(20, 60) === torrent.magnetURI.slice(20, 60));
	if (found) {
		const server = found.createServer();
		server.listen();
		return JSON.stringify({
			address: server.address()
		});
	}
	return JSON.stringify({
		error: "Couldn't find given torrent.... Tell guydht (the developer of this awesome app) something's weird! "
	});
}

function destroyTorrent(torrent) {
	return new Promise((resolve) => {
		const found = client.torrents.find(ele => ele.magnetURI.slice(20, 60) === torrent.magnetURI.slice(20, 60));
		if (found)
			found.destroy(() => resolve(jsonifyTorrent(found)));
		else resolve("{}");
	});
}

function handleEvent(func) {
	return async function wrapper(_, { randomNumber, data }) {
		try {
			let dataToSend = await func(...data);
			ipcRenderer.send("reply" + randomNumber, dataToSend);
		} catch (e) {
			ipcRenderer.send("reply" + randomNumber, undefined);
		}
	};
}

ipcRenderer.on("addTorrent", handleEvent(addTorrent));
ipcRenderer.on("getAllTorrents", handleEvent(getAllTorrents));
ipcRenderer.on("torrentsLength", handleEvent(getTorrentsLength));
ipcRenderer.on("createServer", handleEvent(createServer));
ipcRenderer.on("destroy", handleEvent(destroyTorrent));

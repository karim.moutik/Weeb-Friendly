
const { ipcMain } = require("electron");
exports.init = function init(mainWindow, webTorrentWindow) {
	this.webTorrentWindow = webTorrentWindow;
	this.mainWindow = mainWindow;
	ipcMain.handle("sendToWebtorrentWindow", (_, listenerType, ...data) => {
		return new Promise(resolve => {
			const randomNumber = Math.random();
			this.webTorrentWindow?.webContents.send(listenerType, { randomNumber, data });
			ipcMain.once("reply" + randomNumber, (_, data) => resolve(data));
		}).catch(() => { });
	});
	ipcMain.handle("sendToMainWindow", (_, listenerType, ...data) => {
		return new Promise(resolve => {
			const randomNumber = Math.random();
			this.mainWindow?.webContents.send(listenerType, { randomNumber, data });
			ipcMain.once("reply" + randomNumber, (_, data) => resolve(data));
		}).catch(() => { });
	});
	ipcMain.on("sendToMainWindow", (_, listenerType, ...data) => {
		this.mainWindow?.webContents.send(listenerType, ...data);
	});
	ipcMain.on("sendToWebtorrentWindow", (_, listenerType, ...data) => {
		this.webTorrentWindow?.webContents.send(listenerType, ...data);
	});
	return this;
};

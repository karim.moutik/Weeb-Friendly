import { displayToast } from "../pages/global/ToastMessages";
import AnimeEntry from "./AnimeEntry";

const electron = window.require("electron"),
	fs = window.require("fs");
try {
	window.require("electron-json-config");
}
catch (e) {
	fs.unlinkSync((electron.app || electron.remote.app).getPath('userData') + '/config.json');
}
const storageObject = window.require("electron-json-config"),
	path = window.require("path");
class ThumbnailManager {
	static SAVED_THUMBNAILS_STATE_STORAGE_KEY = "saved-thumbnail-state";
	static SAVED_THUMBNAILS_STATE = storageObject.get(ThumbnailManager.SAVED_THUMBNAILS_STATE_STORAGE_KEY) || false;
	static setThumbnailStorageState(activated: boolean) {
		ThumbnailManager.SAVED_THUMBNAILS_STATE = activated;
		storageObject.set(ThumbnailManager.SAVED_THUMBNAILS_STATE_STORAGE_KEY, activated);
	}

	static SAVED_THUMBNAILS_STORAGE_KEY = "saved-thumbnail-storage";
	static SAVED_THUMBNAILS_PATH = path.join((electron.app || electron.remote.app).getPath("cache"), "/thumbnail-image-storage/");
	static SAVED_THUMBNAILS: Set<number>;
}

function getSavedThumbnails(): Set<number> {
	if (!fs.existsSync(ThumbnailManager.SAVED_THUMBNAILS_PATH))
		fs.mkdirSync(ThumbnailManager.SAVED_THUMBNAILS_PATH);
	const thumbnails = new Set<number>(fs.readdirSync(ThumbnailManager.SAVED_THUMBNAILS_PATH)
		.map((fileName: string) => Number(fileName)).filter((num: number) => !isNaN(num)));
	for (const thumbnail of thumbnails) {
		const fileName = path.join(ThumbnailManager.SAVED_THUMBNAILS_PATH, thumbnail.toString()),
			stats = fs.statSync(fileName);
		if (stats && stats.size <= 453) {
			fs.unlinkSync(fileName);
			thumbnails.delete(thumbnail);
		}
	}
	return thumbnails;
}

if (!ThumbnailManager.SAVED_THUMBNAILS)
	ThumbnailManager.SAVED_THUMBNAILS = getSavedThumbnails();

class CustomCache<K = any, V = any> {
	private inner = new Map<K, [V, number]>();
	private ttlMiliSeconds: number;
	constructor(ttlMiliSeconds: number) {
		this.ttlMiliSeconds = ttlMiliSeconds;
	}

	set(key: K, val: V) {
		this.inner.set(key, [val, Date.now() + this.ttlMiliSeconds]);
	}

	get(key: K): V | undefined {
		const val = this.inner.get(key);
		if (val !== undefined && val[1] > Date.now())
			return val![0];
		return undefined;
	}

	has(key: K) {
		return this.get(key) !== undefined;
	}
};

const storageCache = new CustomCache<string, number>(500); // stores name: malId for caching purposes

let _storageKey = "anime-storage",
	_ANIMES: Map<number, AnimeEntry> = new Map(),
	sync = (anime: AnimeEntry, forceData: boolean = false): AnimeEntry => {
		if (!anime) return new AnimeEntry({});
		let current = get(anime);
		if (!current) {
			if (anime.malId)
				_addToStorage(anime as any);
			return anime;
		}
		if (!forceData)
			current.synonyms.forEach(ele => anime.synonyms.add(ele));
		Object.keys(anime).forEach(key => {
			let value = anime[key as keyof AnimeEntry],
				valueIsTruthy = !(!value && typeof value !== "number");
			if ((forceData || valueIsTruthy) && current![key as keyof AnimeEntry] !== value)
				(current as any)[key] = value;
		});
		_addToStorage(current as any);
		return current;
	},
	get = (anime: AnimeEntry): AnimeEntry | undefined => {
		if (anime.malId)
			return _ANIMES.get(anime.malId);
		if (anime.name) {
			const inCache = storageCache.get(anime.name);
			if (inCache !== undefined)
				return _ANIMES.get(inCache);
			let similar;
			if (anime._synonyms.size)
				similar = [..._ANIMES.values()].filter(ele => ele.compareAnimeNames(anime));
			else
				similar = [..._ANIMES.values()].filter(ele => ele.compareAnimeName(anime.name ?? ""));
			const found = similar.sort((a, b) => a.startDate?.getTime() ?? 0 - (b.startDate?.getTime() ?? 0))[0];
			storageCache.set(anime.name, found?.malId!);
			return found;
		}
	},
	size = () => {
		return _ANIMES.size;
	},
	needsUpdating: number[] = [],
	_addToStorage = (anime: AnimeEntry & { malId: number; }) => {
		_ANIMES.set(anime.malId, anime);
		needsUpdating.push(anime.malId);
	},
	_whenHasInternetStorageKey = "laterWhenIHaveInternet",
	updateInMalWhenHasInternet = (anime: AnimeEntry, updateParams: object) => {
		if (anime.malId) {
			const currentThingsToUpdate: Record<string, object> = storageObject.get(_whenHasInternetStorageKey) || {},
				updateObj: any = currentThingsToUpdate[anime.malId.toString()] || {};
			Object.entries(updateParams).forEach(([key, value]) => {
				updateObj[key] = value;
			});
			currentThingsToUpdate[anime.malId.toString()] = updateObj;
			storageObject.set(_whenHasInternetStorageKey, currentThingsToUpdate);
		}
	},
	lastHasInternetState = false;
setInterval(() => {
	if (needsUpdating.length) {
		let copy: AnimeEntry[] = storageObject.get(_storageKey) || [];
		needsUpdating.forEach(malId => {
			const index = copy.findIndex(ele => ele.malId === malId),
				anime = _ANIMES.get(malId)!;
			const jsonedAnime = anime.readyForJSON();
			for (let key in jsonedAnime) {
				const value = jsonedAnime[key];
				if (value === undefined || value === null || value.length === 0)
					delete jsonedAnime[key];
			}
			if (~index)
				copy[index] = jsonedAnime;
			else
				copy.push(jsonedAnime);
		});
		storageObject.set(_storageKey, copy);
		needsUpdating = [];
	}
	if (lastHasInternetState === false && navigator.onLine) {
		let argsToUpdate: Record<string, object> = storageObject.get(_whenHasInternetStorageKey) || {},
			MALUtils = require("../utils/MAL").default;
		Promise.all(Object.entries(argsToUpdate).map(
			([animeId, updateObj]: any) => MALUtils.updateAnime(new AnimeEntry({ malId: Number(animeId) }), updateObj)
		)).then(ok => {
			if (ok.every(ok => ok)) {
				storageObject.set(_whenHasInternetStorageKey, {});
				if (ok.length)
					displayToast({
						title: "Yay! You have your internet back!",
						body: "Finished updating all your offline progress with MAL :)"
					});
			}
			else
				setTimeout(() => {
					lastHasInternetState = false;
				}, 10000);
		});
	}
	lastHasInternetState = navigator.onLine;
}, 1000);
((storageObject.get(_storageKey) || []) as any[]).forEach(anime => {
	(anime as any).sync = false;
	const animeObject = new AnimeEntry(anime as any);
	_ANIMES.set(animeObject.malId!, animeObject);
});

export { sync, get, size, ThumbnailManager, updateInMalWhenHasInternet };


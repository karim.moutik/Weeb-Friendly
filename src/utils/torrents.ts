import { pantsu, si } from "nyaapi";
import AnimeEntry from "../classes/AnimeEntry";
import Consts from "../classes/Consts";
import DownloadedItem from "../classes/DownloadedItem";
import TorrentManager from "../classes/TorrentManager";
import { handleRelations } from "./general";
import { normalizeAnimeName } from "./OutsourcedGeneral";

const pantsuSearch = pantsu.search,
	siSearch = si.search;

pantsu.search = async (term, ops) => {
	return pantsuSearch(term, ops).catch(() => []);
};

si.search = async (...args) => {
	return siSearch(...args).catch(() => []);
};

const DEFAULT_MAX_RESULTS = 150;
const { parseSync } = window.require("@teidesu/anitomy-js");

function formatBytes(bytes: number, decimals = 2) {
	if (bytes === 0) return '0 Bytes';

	const k = 1024;
	const dm = decimals < 0 ? 0 : decimals;
	const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

	const i = Math.floor(Math.log(bytes) / Math.log(k));

	return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
}

export enum Sources {
	SubsPlease,
	SSA,
	"Erai-raws",
	FreeForAll,
	Any
}

export interface SearchResultExtraInfo {
	description: string;
	size: string;
	fileList: string[];
	comments: {
		text: string,
		author: string,
		authorImage: string,
		date: Date;
	}[];
}

const siPantsuMapping = {
	[Sources.SSA]: {
		si: "SmallSizedAnimations",
		pantsu: "34626"
	},
	[Sources.SubsPlease]: {
		si: "subsplease"
	},
	[Sources["Erai-raws"]]: {
		pantsu: "11974",
		si: "Erai-raws"
	},
	[Sources.FreeForAll]: {
		si: "FreeForAll"
	}
};

export default class TorrentUtils {
	static async search(anime: AnimeEntry, fetchAll: boolean = false, source: Sources = Sources.Any): Promise<SearchResult[]> {
		const results: SearchResult[] = [];
		switch (source) {
			case Sources.Any:
				for (const name of anime.names) {
					results.push(...(await si.search({ term: name, category: si.Category?.ANIME_ENGLISH ?? "1_2" })).map(siResultToSearchResult).filter(ele => !results.some(result => result.link.magnet === ele.link.magnet)));
					results.push(...(await pantsu.search({ term: name, c: pantsu.Category?.ANIME_ENGLISH ?? "3_5" })).map(pantsuResultToSearchResult).filter(ele => !results.some(result => result.link.magnet === ele.link.magnet)));
					if (results.length > 0)
						break;
				}
				return results;
			default:
				return await this.searchSource(anime, fetchAll, source);
		}
	}
	private static async searchSource(anime: AnimeEntry, fetchAll: boolean = false, source: Sources): Promise<SearchResult[]> {
		for (let name of anime.names) {
			try {
				let apiSource: Record<string, string> = (siPantsuMapping as any)[source] || {},
					results = [];
				if (apiSource.si) {
					let sourceValue = apiSource.si;
					if (fetchAll)
						try {
							results.push(...(await si.searchAllByUser(sourceValue, name, { category: si.Category?.ANIME_ENGLISH ?? "1_2" })).map(siResultToSearchResult));
						} catch (e) { console.error(e); }
					else
						try {
							results.push(...(await si.searchByUser(sourceValue, name, DEFAULT_MAX_RESULTS, { category: si.Category?.ANIME_ENGLISH ?? "1_2" })).map(siResultToSearchResult));
						} catch (e) { console.error(e); }
				}
				if (results.length === 0 && apiSource.pantsu) {
					let sourceValue = apiSource.pantsu;
					if (fetchAll)
						try {
							results.push(...(await pantsu.searchAll({
								term: name,
								userID: sourceValue,
								c: pantsu.Category?.ANIME_ENGLISH ?? "3_5"
							})).map(pantsuResultToSearchResult));
						} catch (e) { console.error(e); }
					else
						try {
							results.push(...(await pantsu.search({
								term: name,
								userID: sourceValue,
								c: pantsu.Category?.ANIME_ENGLISH ?? "3_5"
							})).map(pantsuResultToSearchResult));
						} catch (e) { console.error(e); }
				}
				if (results.length === 0 && Object.keys(apiSource).length === 0) {
					if (fetchAll)
						try {
							results.push(...(await si.searchAll(name, { category: si.Category?.ANIME_ENGLISH ?? "1_2" })).map(siResultToSearchResult));
						} catch (e) { console.error(e); }
					else
						try {
							results.push(...(await si.search(name, DEFAULT_MAX_RESULTS, {
								category: si.Category?.ANIME_ENGLISH ?? "1_2"
							})).map(siResultToSearchResult));
						} catch (e) { console.error(e); }
				}
				if (results.length === 0 && Object.keys(apiSource).length === 0) {
					if (fetchAll)
						try {
							results.push(...(await pantsu.searchAll({
								term: name,
								c: pantsu.Category?.ANIME_ENGLISH ?? "3_5"
							})).map(pantsuResultToSearchResult));
						} catch (e) { console.error(e); }
					else
						try {
							results.push(...(await pantsu.search({
								term: name,
								c: pantsu.Category?.ANIME_ENGLISH ?? "3_5"
							})).map(pantsuResultToSearchResult));
						} catch (e) { console.error(e); }
				}
				if (results.length)
					return results;
			}
			catch (e) {
				console.error(e);
			}
		}
		return [];
	}

	static async torrentExtraInfo(torrentPageUrl: string | number): Promise<SearchResultExtraInfo> {
		if (typeof torrentPageUrl === "number") {
			const info = (await pantsu.infoRequest(torrentPageUrl));
			return {
				...info,
				fileList: info.file_list.map(ele => (ele as any).path),
				size: formatBytes(info.filesize),
				comments: info.comments.map(comment => ({
					author: comment.username,
					authorImage: comment.user_avatar,
					date: new Date(comment.date),
					text: comment.content
				}))
			};
		}
		const response = await fetch(torrentPageUrl),
			responseHTML = new DOMParser().parseFromString(await response.text(), 'text/html');
		return {
			description: responseHTML.querySelector("#torrent-description")?.textContent ?? "",
			size: [...responseHTML.querySelectorAll(".panel .row .col-md-5")].find(element => element.previousElementSibling?.textContent === "File size:")?.textContent ?? "",
			fileList: [...responseHTML.querySelectorAll(".torrent-file-list .fa.fa-file")].map(ele => ele.parentNode?.textContent ?? ""),
			comments: [...responseHTML.querySelectorAll(".comment-panel")].map(element => ({
				text: element.querySelector(".comment-content")?.textContent ?? "",
				author: element.querySelector("a[title=\"User\"]")?.textContent ?? "",
				authorImage: element.querySelector("img")?.src ?? "",
				date: new Date(element.querySelector("[data-timestamp]")?.textContent ?? "")
			}))
		};
	}

	static async latest(page = 1, source: Sources = Sources.Any): Promise<SearchResult[]> {
		const results: SearchResult[] = [];
		switch (source) {
			case Sources.Any:
				results.push(...(await si.search(' ', DEFAULT_MAX_RESULTS, { p: page, category: si.Category?.ANIME_ENGLISH ?? "1_2" })).map(siResultToSearchResult));
				results.push(...(await pantsu.search(' ', DEFAULT_MAX_RESULTS, { page, c: pantsu.Category?.ANIME_ENGLISH ?? "3_5" })).map(pantsuResultToSearchResult)
					.filter(ele => !results.some(result => result.link.magnet === ele.link.magnet)));
				return results;
		}
		return await this.getLatestOfSource(page, source);
	}
	private static async getLatestOfSource(page: number, source: Sources): Promise<SearchResult[]> {
		let apiSource: Record<string, string> = (siPantsuMapping as any)[source] ?? {},
			results = [];
		if (apiSource.si)
			try {
				results.push(...(await si.searchByUserAndByPage(apiSource.si, '', page, DEFAULT_MAX_RESULTS, { category: si.Category?.ANIME_ENGLISH ?? "1_2" })).map(siResultToSearchResult));
			} catch (e) { console.error(e); }
		if (apiSource.pantsu)
			try {
				results.push(...(await pantsu.search({
					term: '*',
					userID: apiSource.pantsu,
					page,
					c: pantsu.Category?.ANIME_ENGLISH ?? "3_5"
				})).map(pantsuResultToSearchResult));
			} catch (e) { console.error(e); }
		return results;
	}
}
function siResultToSearchResult(siResult: any): SearchResult {
	const obj = pantsuResultToSearchResult(siResult); //They changed it in the new update to look just like pantsu result!
	obj.link.page = `https://nyaa.si/view/${siResult.id}`;
	return obj;
}

function humanFileSize(size: number) {
	const i = size === 0 ? 0 : Math.floor(Math.log(size) / Math.log(1024));
	return Number((size / Math.pow(1024, i)).toFixed(1)) + ' ' + ['B', 'KiB', 'MiB', 'GiB', 'TiB'][i];
};

function pantsuResultToSearchResult(pantsuResult: any) {
	let categoryMapping: any = {
		"1_2": { label: "English-translated" },
		"6": { label: "Raw" },
		"13": { label: "Non-English-Translated" }
	};
	let result = new SearchResult({
		category: categoryMapping[pantsuResult.sub_category],
		name: pantsuResult.name,
		link: {
			magnet: pantsuResult.magnet,
			page: pantsuResult.id,
			file: pantsuResult.torrent
		},
		fileSize: typeof pantsuResult.filesize === "number" ? humanFileSize(pantsuResult.filesize) : pantsuResult.filesize,
		seeders: Number(pantsuResult.seeders),
		leechers: Number(pantsuResult.leechers),
		timestamp: new Date(pantsuResult.date),
		nbDownload: pantsuResult.completed
	});
	return result;
}


export type EpisodeData = {
	anime_season?: string;
	volume_number: number;
	episodeOrMovieNumber: number;
	year: number;
	quality: number;
	name: string;
	anime_title: string;
	video_resolution?: string;
	release_version?: string;
	episode_number?: string;
	episode_number_alt?: string;
	episode_title?: string;
	anime_year?: string;
	other?: string;
};

export function episodeDataFromFilename(name: string) {
	const obj: EpisodeData = parseSync(name) as any;
	obj.anime_title = normalizeAnimeName(obj.anime_title + (obj.anime_season ? ' s' + Number(obj.anime_season) : ''));
	obj.episodeOrMovieNumber = parseFloat(obj.episode_number?.toString() ?? obj.episode_number_alt?.toString() ?? "");
	obj.volume_number = parseFloat(obj.volume_number?.toString() ?? "");
	obj.year = parseFloat(obj.anime_year?.toString() ?? "");
	obj.quality = obj.video_resolution?.includes("x") ? parseFloat(obj.video_resolution.split("x")[1]) : parseFloat(obj.video_resolution?.toString() ?? "");
	obj.name = `${obj.anime_title}${obj.year ? ` (${obj.year})` : ""}${!isNaN(obj.episodeOrMovieNumber) ? ` Episode ${obj.episodeOrMovieNumber}` : ''}${obj.other ? " (" + obj.other + ")" : ''}`;
	return obj;
}

const eraiSubsRegex = /\[erai-raws]/gi,
	subsPleaseRegex = /\[SubsPlease]/gi,
	ssaRegex = /[[SSA]/gi;

export function findSourceFromFilename(fileName: string): Sources {
	if (fileName.match(eraiSubsRegex))
		return Sources["Erai-raws"];
	if (fileName.match(subsPleaseRegex))
		return Sources.SubsPlease;
	if (fileName.match(ssaRegex))
		return Sources.SSA;
	return Sources.Any;
}

export enum DownloadStatus {
	notDownloaded,
	currentlyDownloading,
	downloaded
}

export class SearchResult {

	constructor(data: Partial<SearchResult>) {
		this.fileSize = data.fileSize ?? "";
		this.leechers = data.leechers ?? -1;
		this.link = data.link ?? {} as any;
		this.name = data.name ?? "";
		this.nbDownload = data.nbDownload ?? -1;
		this.seeders = data.seeders ?? -1;
		this.timestamp = data.timestamp ?? new Date(undefined as any);
		this.episodeData = data.episodeData ?? episodeDataFromFilename(this.name);
		this.animeEntry = data.animeEntry ?? new AnimeEntry({
			name: this.episodeData.anime_title
		});
		this.category = data.category;
		if (handleRelations(this))
			this.name = `${this.animeEntry.name} Episode ${this.episodeData.episodeOrMovieNumber}`;
	}
	category?: {
		label: string;
		code: string;
	};
	fileSize: string;
	leechers: number;
	link: {
		page: string;
		file: string;
		magnet: string;
	};
	name: string;
	nbDownload: number;
	seeders: number;
	timestamp: Date;
	episodeData: EpisodeData;
	animeEntry: AnimeEntry;
	seenThisEpisode() {
		return this.animeEntry && !isNaN(this.episodeData.episodeOrMovieNumber) && this.animeEntry.seenEpisode(this.episodeData.episodeOrMovieNumber);
	}
	static compareEpisodeData = (self: SearchResult, episodeData: DownloadedItem["episodeData"]) => {
		return self.animeEntry.compareAnimeName(episodeData.anime_title) && (
			self.episodeData.name === episodeData.name || self.episodeData.episodeOrMovieNumber === episodeData.episodeOrMovieNumber);
	};
	static compareAnimeEntry = (self: SearchResult, downloadedItem: DownloadedItem) => {
		return self.animeEntry.malId === downloadedItem.animeEntry.malId &&
			downloadedItem.episodeData.episodeOrMovieNumber === self.episodeData.episodeOrMovieNumber;
	};
	get downloadStatus(): DownloadStatus {
		const isDownloaded = () => Consts.DOWNLOADED_ITEMS.some(ele => SearchResult.compareAnimeEntry(this, ele)) || Consts.DOWNLOADED_ITEMS.some(item => SearchResult.compareEpisodeData(this, item.episodeData)),
			isDownloading = () => TorrentManager.getAll().some(torrent => {
				const names = [torrent.name, ...(torrent.files || []).map(ele => ele.name)];
				return names.some(name => SearchResult.compareEpisodeData(this, episodeDataFromFilename(name)));
			});
		return (isDownloaded() ? DownloadStatus.downloaded : isDownloading() ? DownloadStatus.currentlyDownloading : DownloadStatus.notDownloaded);
	}
}

import AnimePage from "./pages/AnimeInfo.tsx";
import Browser from "./pages/Home.tsx";
import Settings from "./pages/Settings";

const config = {
	'/anime/:id': AnimePage,
	'/settings': Settings,
	'/': Browser
};
export default config;

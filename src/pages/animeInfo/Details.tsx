import React, { Component, RefObject, SyntheticEvent } from "react";
import Badge from "react-bootstrap/Badge";
import Button from "react-bootstrap/Button";
import Col from "react-bootstrap/Col";
import FormControl from "react-bootstrap/FormControl";
import Modal from "react-bootstrap/Modal";
import Row from "react-bootstrap/Row";
import { Link } from "react-router-dom";
import { reloadPage } from "../../App";
import AnimeEntry from "../../classes/AnimeEntry";
import Consts from "../../classes/Consts";
import DownloadedItem from "../../classes/DownloadedItem";
import { MALStatuses } from "../../classes/MalStatuses";
import ChangableText from "../../components/ChangableText";
import DownloadedFileThumbnail from "../../components/DownloadedFileThumbnail";
import styles from "../../css/pages/Details.module.css";
import { parseBroadcastToDate } from "../../utils/general";
import MALUtils from "../../utils/MAL";
import { Confirm, formatTimeAgo, hasInternet } from "../../utils/OutsourcedGeneral";
import { AnimeInfoProps } from "../AnimeInfo";
import { displayToast } from "../global/ToastMessages";


export default class Details extends Component<AnimeInfoProps> {

	scoreElement = React.createRef() as RefObject<any>;
	statusElement = React.createRef() as RefObject<any>;
	episodElement = React.createRef() as RefObject<any>;

	render() {
		let sameSeries = Consts.DOWNLOADED_ITEMS.filter(ele => ele.animeEntry.malId === this.props.anime.malId ||
			this.props.anime.compareAnimeName(ele.episodeData.anime_title));
		return (
			<div className={styles.container}>
				{
					this.props.info && (
						<Row className="mt-4" style={{ fontSize: "130%" }}>
							<Col md="auto" className="text-center">
								<Badge>Score</Badge>
								<h5>{this.props.anime.score || "Unknown"}</h5>
								{
									this.props.info.scored_by &&
									<small>{this.props.info.scored_by} Users</small>
								}
							</Col>
							<Col style={{
								display: "flex",
								flexDirection: "column"
							}}>
								<Row>
									<Col>Ranked: <strong>{this.props.info.rank ?? "Unknown"}</strong></Col>
									<Col>Aired: <strong>{this.props.info.airedString}</strong></Col>
									<Col>Members: <strong>{this.props.info.members?.toString() ?? "Unknown"}</strong></Col>
								</Row>
								<Row>
									<Col>Season: <strong>{this.props.info.premiered ?? "Unknown"}</strong></Col>
									<Col>Type: <strong>{this.props.info.type ?? "Unknown"}</strong></Col>
									<Col>Studio: <strong>{this.props.info.studios.length ? this.props.info.studios[0] : "Unknown"}</strong></Col>
								</Row>
								<Row>
									<Col>Status: <strong>{this.props.info.status}</strong></Col>
									<Col>Genres: <strong>{this.props.info.genres.join(", ")}</strong></Col>
									<Col>Popularity: <strong>{this.props.info.popularity ?? "Unknown"}</strong></Col>
								</Row>
								<Row>
									<Col>Duration: <strong>{this.props.info.duration}</strong></Col>
									<Col>Episodes: <strong>{this.props.info.episodes || "Unknown"}</strong></Col>
									<Col>Source: <strong>{this.props.info.source}</strong></Col>
								</Row>
								{
									this.props.info.status === "Currently Airing" && (
										<Row>
											<Col>Next episode {formatTimeAgo(parseBroadcastToDate(this.props.info.broadcast ?? ""),)}</Col>
										</Row>
									)
								}
							</Col>
						</Row>
					)
				}
				<Row>
					<Col style={{ flex: 0 }}>
						Synonyms:
                   </Col>
					<Col>
						{
							[...this.props.anime.synonyms].map((name) => {
								if (name === this.props.anime.name) return null;
								return <ChangableText key={name} text={name} removeButtonTooltipText="Remove Synonyms" onChange={(value: any) => this.changeAnimeSynonyms(name, value)} />;
							})
						}
						<ChangableText key={this.props.anime.synonyms.size} text="+" removeButton={false} onChange={(value: any) => value && this.addAnimeSynonym(value)} onClick={e => (e.target as any).innerHTML = ""} />
					</Col>
				</Row>
				{
					Consts.MAL_USER.isLoggedIn && Consts.MAL_USER.animeList.all[this.props.anime.malId!] ? (
						<div>
							<Row>
								<Col className="mt-3">
									<h3>{Consts.MAL_USER.username}'s status of {this.props.anime.name}:</h3>
								</Col>
							</Row>
							<Row>
								<Col>
									<FormControl key={this.props.anime.myMalStatus} onChange={this.updateAnime.bind(this)} ref={this.statusElement}
										as="select" className="w-auto" defaultValue={this.props.anime.myMalStatus as any}>
										{
											Object.keys(MALStatuses).filter(ele => isNaN(Number(ele))).map(status => (
												<option key={status} value={(MALStatuses as any)[status]}>{status}</option>
											))
										}
									</FormControl>
								</Col>
								<Col>
									<FormControl key={this.props.anime.myMalRating} onChange={this.updateAnime.bind(this)} ref={this.scoreElement} as="select"
										className="w-auto" defaultValue={(this.props.anime.myMalRating || 0).toString()}>
										<option value="0">Select</option>
										{
											AnimeEntry.SCORES.map((score, i) =>
												<option key={score} value={i + 1}>{`(${i + 1}) ${score}`}</option>
											)
										}
									</FormControl>
								</Col>
								<Col>
									<FormControl
										key={this.props.anime.myWatchedEpisodes}
										type="number"
										max={this.props.anime.totalEpisodes || undefined}
										min={0}
										style={{ background: "transparent", width: "1.5em" }}
										plaintext
										onChange={this.updateAnime.bind(this)}
										className="d-inline guydhtNoSpinner"
										ref={this.episodElement}
										defaultValue={(this.props.anime.myWatchedEpisodes || 0).toString()} />/{this.props.anime.totalEpisodes || "?"}
								</Col>
								<Col>
									<span className={styles.removeAnimeButton} onClick={() => this.removeAnime(this.props.anime)}>Remove From List</span>
								</Col>
							</Row>
						</div>
					) : <Row>
							{
								Consts.MAL_USER.isLoggedIn && hasInternet() ? (
									<Button onClick={() => this.addAnime(this.props.anime)}>Add to MyAnimeList</Button>
								) : Consts.MAL_USER.isLoggedIn && (
									<span>
										Can't add to MyAnimeList since you dont have internet connectivity!
									</span>
								)
							}
						</Row>
				}
				<Row>
					<Modal.Dialog className={styles.modal}>
						<Modal.Header>
							<Modal.Title>Synopsis</Modal.Title>
						</Modal.Header>

						<Modal.Body>
							<p>
								{
									this.props.info.synopsis
								}
							</p>
						</Modal.Body>
					</Modal.Dialog>
				</Row>
				{
					!!Object.values(this.props.info.related).length &&
					<Row>
						<Modal.Dialog className={styles.modal}>
							<Modal.Header>
								<Modal.Title>Related</Modal.Title>
							</Modal.Header>

							<Modal.Body>
								{
									Object.entries(this.props.info.related).map(([type, data]) => {
										return (
											<p key={type}>
												{type}: {
													data.map((ele: any) => {
														if (ele.type === "anime")
															return (
																<Link key={ele.mal_id} className="mr-3" to={{
																	pathname: "/anime/" + ele.mal_id,
																	state: {
																		animeEntry: new AnimeEntry({
																			name: ele.name,
																			malId: ele.mal_id
																		})
																	}
																}}>
																	{ele.name}
																</Link>
															);
														return `${ele.name} (${ele.type})`;
													})
												}
											</p>
										);
									})
								}
							</Modal.Body>
						</Modal.Dialog>
					</Row>
				}
				{
					sameSeries.length > 0 && (
						<Row>
							<Modal.Dialog className={styles.modal}>
								<Modal.Header>
									<Modal.Title>Downloaded Episodes</Modal.Title>
									<Button className="float-right" onClick={() => this.deleteSeriesEpisodes(sameSeries)} >Delete Episodes</Button>
								</Modal.Header>
								<Modal.Body style={{
									display: "grid",
									gridTemplateColumns: "1fr 1fr 1fr 1fr 1fr 1fr",
									gridGap: ".5rem"
								}}>
									{
										sameSeries.sort((a, b) => a.episodeData.episodeOrMovieNumber - b.episodeData.episodeOrMovieNumber).map(ele =>
											<DownloadedFileThumbnail key={ele.absolutePath} downloadedItem={ele} />
										)
									}
								</Modal.Body>
							</Modal.Dialog>
						</Row>
					)
				}
			</div >
		);
	}

	async deleteSeriesEpisodes(episodes: DownloadedItem[]) {
		const ok = await new Promise(resolve => Confirm(`are you sure you want to delete all downloaded episodes of ${this.props.anime.name}?`, resolve));
		if (ok) {
			await Promise.allSettled(episodes.map(async episode => await episode.delete(false)));
			await Consts.reloadDownloads();
			reloadPage();
		}
	}

	updateTimeout?: number;
	static UPDATE_TIMEOUT_MS = 2000;
	removeAnime(anime: AnimeEntry) {
		MALUtils.removeAnime(anime as any).then(ok => {
			if (ok) {
				this.forceUpdate();
				displayToast({ title: 'Successfully Rmoved!', body: `Succesfully removed ${this.props.anime.name} from your MAL list!` });
			}
			else
				displayToast({ title: 'Failed To Remove!', body: `Failed removing ${this.props.anime.name} from your MAL list! Maybe try again later?` });
		});
	}
	addAnime(anime: AnimeEntry) {
		const onError = () => {
			displayToast({ title: 'Error!', body: "Couldn't add to Myanimelist... Maybe check its status, and if everything seems fine, contact guydht!" });
		};
		MALUtils.addAnime(anime as any).then(ok => ok ? this.forceUpdate() : onError()).catch(onError);
	}
	updateAnime(e: SyntheticEvent) {
		if (!this.statusElement.current || !this.episodElement.current || !this.scoreElement.current) return;
		clearTimeout(this.updateTimeout);
		switch (e.currentTarget) {
			case this.episodElement.current:
				switch (Number(this.episodElement.current.value)) {
					case this.props.anime.totalEpisodes:
						this.statusElement.current.value = MALStatuses.Completed;
						break;
					case 0:
						this.statusElement.current.value = MALStatuses["Plan To Watch"];
						break;
					default:
						this.statusElement.current.value = MALStatuses.Watching;
				};
				break;
			case this.statusElement.current:
				switch (Number(this.statusElement.current.value)) {
					case MALStatuses.Completed:
						this.episodElement.current.value = this.props.anime.totalEpisodes;
						break;
					case MALStatuses["Plan To Watch"]:
						this.episodElement.current.value = 0;
				};
		};
		this.updateTimeout = window.setTimeout(() => {
			if (!this.statusElement.current || !this.episodElement.current || !this.scoreElement.current) return;
			MALUtils.updateAnime(this.props.anime as any, {
				episodes: Number(this.episodElement.current.value),
				status: Number(this.statusElement.current.value),
				score: Number(this.scoreElement.current.value)
			}).then(ok => {
				if (ok)
					displayToast({ title: 'Successfully updated!', body: `Succesfully update ${this.props.anime.name}!` });
				else if (hasInternet())
					displayToast({ title: 'Something Went Wrong!', body: `MyanimeList sent error code :(\nTry logging in again!` });
				reloadPage();
			});
		}, Details.UPDATE_TIMEOUT_MS);
	}
	addAnimeSynonym(synonym: string) {
		this.props.anime.syncGet();
		this.props.anime.synonyms.add(synonym);
		this.props.anime.syncPut(true);
		this.forceUpdate();
	}
	changeAnimeSynonyms(synonymToChange: string, newSynonym: string) {
		this.props.anime.syncGet();
		this.props.anime.synonyms.delete(synonymToChange);
		if (newSynonym && newSynonym.trim())
			this.props.anime.synonyms.add(newSynonym);
		this.props.anime.syncPut(true);
		this.forceUpdate();
	}
}

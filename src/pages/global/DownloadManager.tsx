import HumanizeDuration from "humanize-duration";
import React, { Component } from "react";
import Button from "react-bootstrap/Button";
import ButtonGroup from "react-bootstrap/ButtonGroup";
import Col from "react-bootstrap/Col";
import Container from "react-bootstrap/Container";
import Image from "react-bootstrap/Image";
import ListGroup from "react-bootstrap/ListGroup";
import ProgressBar from "react-bootstrap/ProgressBar";
import Row from "react-bootstrap/Row";
import refreshIcon from "../../assets/refresh.png";
import DownloadedItem from "../../classes/DownloadedItem";
import TorrentManager, { Torrent } from "../../classes/TorrentManager";
import CloseButton from "../../components/CloseButton";
import MovableComponent from "../../components/MovableComponent";
import styles from "../../css/pages/DownloadManager.module.css";
import { displayModal } from "./ModalViewer";
import { displayToast } from "./ToastMessages";

class DisplayTorrent extends Component<{
	torrent: Torrent;
}> {

	doUpdate = true;
	updateTimeout = 0;

	state = {
		torrent: this.props.torrent,
		refreshing: false
	};

	hasDownloadListener = false;

	onDownload = (torrent: Torrent) => {
		if (!this.doUpdate) return;
		this.updateTimeout = setTimeout(() => this.doUpdate = true, 150) as unknown as number;
		window.requestAnimationFrame(() => this.setState({ torrent }));
		this.doUpdate = false;
	};

	componentDidMount() {
		TorrentManager.addTorrentListener(this.state.torrent, TorrentManager.Listener.torrentUpdated, this.onDownload);
		this.hasDownloadListener = true;
	}

	componentWillUnmount() {
		TorrentManager.removeTorrentListener(this.state.torrent, TorrentManager.Listener.torrentUpdated, this.onDownload);
	}

	render() {
		if (!this.state.torrent.name || !this.state.torrent.magnetURI) return null;
		if (!this.state.torrent.files) return (
			<ListGroup.Item>
				<h5>
					{this.state.torrent.name}
				</h5>
				<Row>
					<Col>
						<Button disabled>
							On Hold
					</Button>
					</Col>
					<Col>
						<Button onClick={() => this.removeTorrent()}>
							Cancel Torrent
						</Button>
					</Col>
				</Row>
			</ListGroup.Item>
		);
		const restartTorrent = async () => {
			if (this.state.refreshing) return;
			this.setState({
				refreshing: true
			});
			await this.pauseTorrent();
			await this.resumeTorrent();
			this.setState({
				refreshing: false
			});
		};
		return (
			<ListGroup.Item className={styles.torrentEntry}>
				<h5>
					{this.state.torrent.name}
					<small className={styles.refreshIcon + (this.state.refreshing ? " " + styles.refreshing : "")}>
						<Image src={refreshIcon} onClick={restartTorrent} />
					</small>
				</h5>
				<div className={styles.threeColumnRow}>
					<div>
						{
							this.state.torrent.destroyed ||
							<small>
								{downloadSpeedText(Number(this.state.torrent.downloadSpeed.toFixed(2)))}
								<br />
								{HumanizeDuration(this.state.torrent.timeRemaining, {
									round: true,
									largest: 3,
									serialComma: false
								})}
							</small>
						}
					</div>
					<div>
						{
							this.state.torrent.destroyed ||
							<small>
								Peers: {this.state.torrent.numPeers}
							</small>
						}
					</div>
					<div>
						<ButtonGroup>
							<Button onClick={() => this.state.torrent.destroyed ? this.resumeTorrent() : this.pauseTorrent()}>
								{this.state.torrent.destroyed ? "Resume" : "Pause"}
							</Button>
							<Button onClick={() => this.playTorrent()}>
								Play Torrent
						</Button>
							<Button onClick={() => this.removeTorrent()}>
								Cancel Torrent
						</Button>
						</ButtonGroup>
					</div>
				</div>
				<Row className="mt-2">
					<ProgressBar style={{ width: "100%" }}
						now={this.state.torrent.progress * 100}
						label={`${downloadSizeText(this.state.torrent.downloaded)} / ${downloadSizeText(this.state.torrent.length)} (${Number((this.state.torrent.progress * 100).toFixed(2))}%)`} />
				</Row>
			</ListGroup.Item>
		);
	}
	server?: any;
	async playTorrent() {
		if (!this.state.torrent.files.length) return displayToast({ title: "Couldn't start playnig torrent", body: "Can't find downloaded files!" });
		if (!this.server)
			this.server = await TorrentManager.serverOfTorrent(this.state.torrent);
		if (this.server.error) {
			delete this.server;
			return displayToast({ title: "Error playing torrent!", body: this.server.error });
		}
		const address = this.server.address,
			files = this.state.torrent.files.map((ele, index) => ([index, ele] as [number, typeof ele])).sort((a, b) => a[1].name.localeCompare(b[1].name));
		let chosenIndex = 0;
		if (this.state.torrent.files.length > 1)
			await displayModal({
				title: "Please Choose which file to play",
				body: (
					<select onChange={(event: React.ChangeEvent) => chosenIndex = Number((event.target as any).value)} defaultValue={chosenIndex}>
						{files.map(([index, file]) => (
							<option key={file.name} value={index}>
								{file.name}
							</option>
						))}
					</select>
				)
			});
		const url = `http://localhost:${address.port}/${chosenIndex}`;
		const videoItem = new DownloadedItem(url ?? "", this.state.torrent.files[chosenIndex].name, new Date());
		videoItem.videoSrc = url;
		videoItem.startPlaying();
	}
	removeTorrent() {
		TorrentManager.remove(this.state.torrent);
	}
	async pauseTorrent() {
		this.setState({ torrent: await TorrentManager.destroy(this.state.torrent) });
	}
	async resumeTorrent() {
		this.setState({
			torrent: await TorrentManager.resume(this.state.torrent)
		});
	}
}

export default class DownloadManager extends Component {

	state: { hideFlag: boolean; showOpener: boolean; } = {
		hideFlag: false,
		showOpener: true
	};

	container = React.createRef<any>();

	static torrentSorter(a: Torrent, b: Torrent) {
		return a.files && b.files && a.name && b.name ? a.name.localeCompare(b.name) : 0;
	}

	listeners: Set<string> = new Set();

	hideOpenerInterval: number = -1;

	doUpdate = () => this.forceUpdate();

	componentDidMount() {
		TorrentManager.addEventListener(TorrentManager.Listener.addedTorrent, this.doUpdate);
		TorrentManager.addEventListener(TorrentManager.Listener.removedTorrent, this.doUpdate);
		this.hideOpenerInterval = setInterval(() => {
			const showOpener = !document.elementsFromPoint(10, 0).some(ele => ele.tagName === "VIDEO");
			if (showOpener !== this.state.showOpener)
				this.setState({
					showOpener
				});
		}, 1000) as unknown as number;
	}

	componentWillUnmount() {
		clearInterval(this.hideOpenerInterval);
		TorrentManager.removeEventListener(TorrentManager.Listener.addedTorrent, this.doUpdate);
		TorrentManager.removeEventListener(TorrentManager.Listener.removedTorrent, this.doUpdate);
	}

	render() {
		const torrents = TorrentManager.getAll().sort(DownloadManager.torrentSorter),
			hide = () => {
				this.container.current!.style.transform = "scaleY(0)";
				this.setState({
					hideFlag: true
				});
			},
			show = () => {
				this.container.current!.style.transform = "scaleY(1)";
				this.setState({
					hideFlag: false
				});
			};
		if (!torrents.length)
			return null;
		return (
			<MovableComponent
				style={{ position: "fixed", top: 0, left: 10, height: this.state.hideFlag ? 0 : "auto", zIndex: 2029 }}>
				<span
					onPointerEnter={show}
					style={{ position: "absolute", display: this.state.showOpener ? "" : "none" }}>
					View Current Downloads
                </span>
				<Container
					ref={this.container}
					className="p-0"
					style={{ zIndex: this.state.hideFlag ? -1 : 999, transition: "transform 0.5s", overflow: "hidden", transform: "scaleY(1)", transformOrigin: '0 0' }}>
					<CloseButton onClick={hide} className="mr-2 mt-1 p-1" />
					<ListGroup className={styles.grid + " " + styles["grid-template-1"]}>
						{
							torrents.map(torrent => <DisplayTorrent key={torrent.id} torrent={torrent} />)
						}
					</ListGroup>
				</Container>
			</MovableComponent>
		);
	}
}
function downloadSizeText(bytes: number): string {
	if (bytes > 1024 * 1024 * 1024)
		return `${Number((bytes / 1024 / 1024 / 1024).toFixed(2))} GB`;
	if (bytes > 1024 * 1024)
		return `${Number((bytes / 1024 / 1024).toFixed(2))} MB`;
	if (bytes > 1024)
		return `${Number((bytes / 1024).toFixed(2))} KB`;
	return `${bytes || 0} B`;
}
function downloadSpeedText(bytesPerSecond: number): string {
	return downloadSizeText(bytesPerSecond) + "/s";
}

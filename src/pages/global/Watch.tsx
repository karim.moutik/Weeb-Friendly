import React, { Component } from "react";
import Jumbotron from "react-bootstrap/Jumbotron";
import { reloadPage, setAppState } from "../../App";
import Consts from "../../classes/Consts";
import DownloadedItem from "../../classes/DownloadedItem";
import { loadingComponent } from "../../components/LoadingComponent";
import MovableComponent from "../../components/MovableComponent";
import VideoPlayer from "../../components/VideoPlayer/VideoPlayer";
import styles from "../../css/pages/WatchPlayer.module.css";
import MALUtils from "../../utils/MAL";
import { Confirm, hasInternet } from "../../utils/OutsourcedGeneral";
import AnimeInfo from "../AnimeInfo";
import { displayToast } from "./ToastMessages";

export default class Watch extends Component<{ downloadedItem?: DownloadedItem; }> {

	static UPDATE_ANIME_PROGRESS_THRESHOLD = 0.95;
	wantsToUpdateInMAL = true;

	state: {
		showingVideo: boolean;
		videoOpacity: number;
		showAnimePage: boolean,
		preventScroll: boolean;
		searchingVideonameInMAL: boolean;
	} = {
			showingVideo: true,
			videoOpacity: 1,
			showAnimePage: false,
			preventScroll: false,
			searchingVideonameInMAL: false
		};

	removingVideo = false;
	movingElement = React.createRef<MovableComponent>();
	checkForProgressInterval = 0;

	componentDidMount() {
		this.changedProps();
		window.addEventListener("webkitfullscreenchange", this.bindedFullScreenListener);
		document.addEventListener("pointerlockchange", this.bindedPointerLockListener);
		const progressFromLocalStorage = () => {
			if (!this.props.downloadedItem) return;
			let obj = JSON.parse(localStorage.getItem("videoLastTime") || "{}"),
				relevant = obj[this.props.downloadedItem.episodeData.name];
			if (relevant)
				return relevant[1].progress;
			return 0;
		};
		this.checkForProgressInterval = setInterval(() => {
			if (!this.props.downloadedItem) return;
			if (!this.props.downloadedItem.animeEntry.malId
				|| (this.props.downloadedItem.animeEntry.myWatchedEpisodes || 0) >= this.props.downloadedItem.episodeData.episodeOrMovieNumber) return;
			let current = progressFromLocalStorage();
			if (current <= Watch.UPDATE_ANIME_PROGRESS_THRESHOLD)
				this.wantsToUpdateInMAL = true;
			if (this.wantsToUpdateInMAL && current > Watch.UPDATE_ANIME_PROGRESS_THRESHOLD &&
				this.props.downloadedItem.episodeData.episodeOrMovieNumber !== this.props.downloadedItem.animeEntry.myWatchedEpisodes) {
				this.wantsToUpdateInMAL = false;
				const updateEpisodeInMal = () => {
					MALUtils.UpdateWatchedEpisode(this.props.downloadedItem!).then(ok => {
						ok ? displayToast({
							title: "Anime Updated Successfully",
							body: `Successfully updated ${this.props.downloadedItem!.episodeData.name} in MAL!`
						}) : hasInternet() && displayToast({
							title: "Failed updating Anime",
							body: `Failed updating ${this.props.downloadedItem!.episodeData.name} in MAL! Try logging in again!`
						});
						reloadPage();
					});
				};
				if (Consts.AUTO_UPDATE_IN_MAL && (
					(this.props.downloadedItem.animeEntry.myWatchedEpisodes ?? 0) < this.props.downloadedItem.episodeData.episodeOrMovieNumber
				))
					updateEpisodeInMal();
				else if (Consts.MAL_USER.isLoggedIn)
					Confirm(`Do you want to update ${this.props.downloadedItem.animeEntry.name} to episode ${this.props.downloadedItem.episodeData.episodeOrMovieNumber
						} in MAL?`, (ok: boolean) => {
							if (ok) updateEpisodeInMal();
						});
			}
		}, 500) as unknown as number;
	}

	componentWillUnmount() {
		window.removeEventListener("webkitfullscreenchange", this.bindedFullScreenListener);
		document.removeEventListener("pointerlockchange", this.bindedPointerLockListener);
		clearInterval(this.checkForProgressInterval);
	}

	fullScreenListener() {
		this.setState({
			showAnimePage: document.fullscreenElement !== null
		});
	}

	bindedFullScreenListener = this.fullScreenListener.bind(this);

	pointerLockListener() {
		this.setState({
			preventScroll: document.pointerLockElement !== null
		});
	}

	bindedPointerLockListener = this.pointerLockListener.bind(this);

	changedProps() {
		if (!this.props.downloadedItem) return;
		this.wantsToUpdateInMAL = true;
		this.props.downloadedItem.animeEntry.syncGet();
		if (!this.props.downloadedItem.animeEntry.malId) {
			this.setState({ searchingVideonameInMAL: true });
			this.props.downloadedItem.searchInMal().then(() => this.setState({ searchingVideonameInMAL: false }));
		}
	}

	componentDidUpdate(prevProps: Watch["props"]) {
		if (this.removingVideo)
			return this.removingVideo = false;
		if (!this.state.showingVideo && !this.removingVideo) {
			this.setState({
				showingVideo: true
			});
		}
		if (prevProps.downloadedItem?.absolutePath !== this.props.downloadedItem?.absolutePath)
			this.changedProps();
	};

	hide = (e: React.MouseEvent) => {
		this.setState({
			videoOpacity: 0
		});
		e.stopPropagation();
		setTimeout(() => {
			this.removingVideo = true;
			this.setState({
				showingVideo: false,
				videoOpacity: 1
			});
			setAppState({
				videoItem: null
			});
		}, 500);
	};

	render() {
		if (!this.state.showingVideo)
			return null;
		const styleObject: Record<string, any> = { transition: "opacity .5s", opacity: this.state.videoOpacity, position: "fixed", zIndex: 7, display: this.props.downloadedItem === null ? "none" : "" };
		for (const key in Consts.WATCH_PLAYER_SIZE)
			styleObject[key] = Consts.WATCH_PLAYER_SIZE[key];
		return (
			<MovableComponent
				onResizeFinish={this.onMoveFinish.bind(this)}
				onDragFinish={this.onMoveFinish.bind(this)}
				ref={this.movingElement}
				style={styleObject}
				resizable={true}>
				<span
					style={{ position: "absolute", zIndex: 2, right: 0, cursor: "pointer" }}
					className="mr-2 mt-1 p-1" onClick={this.hide}>
					<span aria-hidden="true">×</span>
				</span>
				{
					this.props.downloadedItem &&
					<VideoPlayer
						style={{ position: this.state.showAnimePage ? "fixed" : "initial", overflowY: this.state.preventScroll ? "hidden" : "auto" }}
						downloadedItem={this.props.downloadedItem}
						as={Jumbotron}
						className={styles.container}
						src={this.props.downloadedItem.videoSrc || Consts.FILE_URL_PROTOCOL + this.props.downloadedItem.absolutePath}>
						<div className={(this.state.showAnimePage ? styles.animeInfo : "d-none") + " mx-5 px-5 mt-5"}>
							{
								this.props.downloadedItem.animeEntry.malId ? (
									<AnimeInfo
										key={this.props.downloadedItem.animeEntry.malId}
										anime={this.props.downloadedItem.animeEntry} />
								) : (this.state.searchingVideonameInMAL ? loadingComponent.loadingElement : `Couldn't find ${this.props.downloadedItem.episodeData.name} in MAL`)
							}
						</div>
					</VideoPlayer>
				}
			</MovableComponent>
		);
	}
	onMoveFinish(_: any, didMoveInGesture: boolean) {
		if (didMoveInGesture && this.movingElement.current && this.movingElement.current.element.current) {
			let rect = (this.movingElement.current.element.current.getBoundingClientRect() as DOMRect).toJSON();
			delete rect.bottom;
			delete rect.right;
			delete rect.x;
			delete rect.y;
			Consts.setWatchPlayerSize(rect);
		}
	}
}

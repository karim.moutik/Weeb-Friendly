import React, { Component } from "react";
import ReactDom from "react-dom";
import { ReactComponent as NextEpisodeIcon } from "../../assets/NextIcon.svg";
import Consts from "../../classes/Consts";
import styles from "../../css/components/VideoPlayer.module.css";
import { asd } from "../../jsHelpers/jifa";
import SubtitlesOctopus from "../../jsHelpers/subtitleParsers/Octopus";
import { DisplayDownloadedAnime } from "../../pages/home/DownloadedAnime";
import { convertSrtToASS, groupBySeries } from "../../utils/general";
import { CacheLocalStorage, compareAnimeName } from "../../utils/OutsourcedGeneral";
import VideoThumbnail, { renderVideo } from "../VideoThumbnail";
import Comments from "./Comments";
import { CountdownToNextEpisode } from "./Countdown";
import { Playlist } from "./Playlist";

const { ipcRenderer } = window.require("electron");

class AdjacentEpisodeButton extends Component {

	static SIZE_PERCENTAGE_OF_CONTAINER = .5;

	isPrev = null;

	state = {
		showThumbnail: false
	};

	iconRef = React.createRef();

	render() {
		const hideThumbnail = () => this.setState({ showThumbnail: false }),
			showThumbnail = () => this.setState({ showThumbnail: true }),
			props = { ...this.props };
		delete props.downloadedItem;
		delete props.videoContainer;
		delete props.title;
		delete props.thumbnailMarginLeft;
		delete props.children;
		return (
			<div className={styles.nextEpisodeIcon} {...props}>
				<NextEpisodeIcon style={{ transform: this.isPrev ? "rotate(180deg)" : "" }}
					onMouseEnter={showThumbnail} onMouseLeave={hideThumbnail} ref={this.iconRef} />
				<div className={styles.thumbnailCanvasContainer + (this.state.showThumbnail ? "" : ` ${styles.hidden}`)} style={{
					height: this.props.videoContainer.clientHeight * (NextEpisodeButton.SIZE_PERCENTAGE_OF_CONTAINER / 2),
					width: this.props.videoContainer.clientWidth * NextEpisodeButton.SIZE_PERCENTAGE_OF_CONTAINER,
					marginLeft: this.props.thumbnailMarginLeft
				}}
					onMouseEnter={showThumbnail} onMouseLeave={hideThumbnail}>
					<VideoThumbnail className={styles.thumbnailCanvas} key={this.props.downloadedItem.absolutePath} videoUrl={Consts.FILE_URL_PROTOCOL + this.props.downloadedItem.absolutePath} />
					<div className={styles.thumbnailCanvasTitle}>
						<strong>
							{this.isPrev ? "Prev (Shift + B)" : "Next (Shift + N)"}
						</strong>
						<span>
							{this.props.title}
						</span>
					</div>
				</div>
			</div>
		);
	}

}

class NextEpisodeButton extends AdjacentEpisodeButton {
	isPrev = false;
}

class PrevEpisodeButton extends AdjacentEpisodeButton {
	isPrev = true;
}

export default class VideoPlayer extends Component {

	videoWrapper = React.createRef();
	subtitlesOctopus;
	videoHandler;
	subtitlesMap = {};

	state = {
		displayFinishScreenEntries: false,
		seriesQueue: []
	};

	indexInSeriesQueue = {};

	getIndexInSeriesQueue() {
		let seriesIndex, episodeIndex;
		for (const sIndex in this.state.seriesQueue)
			for (const eIndex in this.state.seriesQueue[sIndex])
				if (this.state.seriesQueue[sIndex][eIndex].episodeData.name === this.props.downloadedItem.episodeData.name) {
					seriesIndex = sIndex;
					episodeIndex = eIndex;
				}
		this.indexInSeriesQueue = {
			series: {
				total: this.state.seriesQueue.length,
				index: Number(seriesIndex)
			},
			episode: {
				total: this.state.seriesQueue[seriesIndex]?.length,
				index: Number(episodeIndex)
			}
		};
		return this.indexInSeriesQueue;
	}

	componentWillUnmount() {
		ipcRenderer.removeListener("sub-file-processed", this.handleProcessedSubs);
		ipcRenderer.send("stop-processing-of-sub-file");
		let video = this.videoWrapper.current.querySelector("video");
		new CacheLocalStorage("videoLastTime").setItem(this.props.downloadedItem.episodeData.name, { currentTime: video.currentTime, progress: video.currentTime / video.duration });
		if (this.subtitlesOctopus)
			try {
				this.subtitlesOctopus.dispose();
			} catch (e) { }
			finally {
				clearInterval(this.subtitlesOctopus.resizeInterval);
				delete this.subtitlesOctopus;
			}
		if (this.videoHandler) {
			document.body.removeEventListener("keydown", this.videoHandler.handleKeyDown);
			this.videoHandler.destroy();
		}
		if (document.fullscreenElement)
			document.exitFullscreen();
	}

	componentDidMount() {
		this.createWatchQueue();
		this.getIndexInSeriesQueue();
		this.setupVideo();
		ipcRenderer.addListener("sub-file-processed", this.handleProcessedSubs);
		const container = this.videoWrapper.current;
		container.addEventListener("guydhtChangeSubs", event => {
			this.subtitlesOctopus.setTrack(this.subtitlesMap[event.detail].data);
		});
	}

	handleProcessedSubs = (_, files) => {
		this.handleSubs(files);
	};

	createWatchQueue() {
		const series = groupBySeries(Consts.FILTERED_DOWNLOADED_ITEMS).filter(ele => ele[0]?.episodeData.anime_title);
		const orderedSeries = series.sort((a, b) => {
			return Math.max(...b.map(ele => ele.lastUpdated.getTime())) -
				Math.max(...a.map(ele => ele.lastUpdated.getTime()));
		}).map(series => series.filter(episode => !episode.seenThisEpisode() || episode.episodeData.name === this.props.downloadedItem.episodeData.name).sort((a, b) => a.episodeData.episodeOrMovieNumber - b.episodeData.episodeOrMovieNumber)).filter(ele => ele.length),
			indexOfCurrentSeries = orderedSeries.findIndex(series => series[0]?.episodeData.anime_title === this.props.downloadedItem.episodeData.anime_title);
		if (indexOfCurrentSeries >= 0)
			orderedSeries.splice(0, 0, orderedSeries.splice(indexOfCurrentSeries, 1)[0]);
		//eslint-disable-next-line
		this.state.seriesQueue = orderedSeries; // Since I need it to be updated instantly
		this.setState({
			seriesQueue: orderedSeries
		});
	}


	handleSubs = async (subFiles, timeOffset = 0) => {
		const fonts = [],
			defaultSub = subFiles[0],
			container = this.videoWrapper.current;
		for (let f of subFiles) {
			if ((f.name.endsWith(".ass") || f.name.endsWith(".ssa"))) {
				this.subtitlesMap[f.title] = f;
			}
			else if (f.name.endsWith(".ttf"))
				fonts.push(URL.createObjectURL(new Blob([f.data])));
			else if (f.name.endsWith(".srt")) {
				f.data = await convertSrtToASS(f.data);
				this.subtitlesMap[f.title] = f.data;
			}
		}
		var options = {
			canvas: container.querySelector("canvas"),
			subContent: defaultSub?.data,
			fonts,
			timeOffset,
			workerUrl: "./OctopusWorker.js",
		};
		const subtitleNames = Object.keys(this.subtitlesMap),
			video = container.querySelector("video");
		if (this.subtitlesOctopus)
			this.subtitlesOctopus.timeOffset = timeOffset;
		if (defaultSub && !this.subtitlesOctopus) {
			this.subtitlesOctopus = new SubtitlesOctopus(options);
			this.subtitlesOctopus.resize(window.screen.width, window.screen.height);
			const syncSubtitleOctopus = () => {
				if (this.subtitlesOctopus)
					this.subtitlesOctopus.setCurrentTime(video.currentTime);
			};
			video.addEventListener("timeupdate", syncSubtitleOctopus);
			video.addEventListener("seeked", syncSubtitleOctopus);
		}
		if (defaultSub) {
			this.subtitlesOctopus.setTrack(defaultSub.data);
			container.setSubtitleTracksNames(subtitleNames);
		}
		else if (timeOffset === 0) {
			this.subtitlesOctopus.freeTrack();
		}
	};

	async setupVideo() {
		let container = this.videoWrapper.current,
			handleKeyDown = e => {
				const [prevEpisode, nextEpisode] = this.getAdjacentDownloadedItems();
				if (e.shiftKey && !e.ctrlKey && !e.altKey && e.code === "KeyN" && nextEpisode)
					nextEpisode.startPlaying();
				else if (e.shiftKey && !e.ctrlKey && !e.altKey && e.code === "KeyP" && prevEpisode)
					prevEpisode.startPlaying();
			};
		document.body.addEventListener("keydown", handleKeyDown);
		this.videoHandler = asd(this.props.downloadedItem.episodeData.name, container, this.props.src, (file, timeOffset) => this.handleSubs([file].filter(ele => ele), timeOffset));
		this.videoHandler.handleKeyDown = handleKeyDown;
		let video = container.querySelector("video");
		ipcRenderer.send("process-subs-of-file", this.props.src);
		video.addEventListener("ended", () => {
			if (video.currentTime === video.duration)
				this.setState({ displayFinishScreenEntries: true });
		});
		video.addEventListener('error', () => {
			if (video.src.startsWith('http')) {
				const found = Consts.DOWNLOADED_ITEMS.find(item => item.equals(this.props.downloadedItem));
				if (found)
					found.startPlaying();
			}
		});
		let removeFinishScreen = () => {
			if (video.currentTime !== video.duration)
				this.setState({ displayFinishScreenEntries: false });
		};
		video.addEventListener("playing", removeFinishScreen);
		video.addEventListener("seeking", removeFinishScreen);
		ReactDom.render(<Comments episode={this.props.downloadedItem} />, container.querySelector("#guydhtVideoCommentsContainer"));
		const subs = await ipcRenderer.invoke("get-subs-of-file", this.props.downloadedItem.absolutePath);
		if (subs?.length)
			this.handleSubs(subs);
	}

	getAdjacentDownloadedItems() {
		const position = this.indexInSeriesQueue,
			prevEpisodeIndex = position.episode.index > 0 ? [position.series.index, position.episode.index - 1] :
				position.series.index > 0 ? [position.series.index - 1, 0] : [],
			nextEpisodeIndex = position.episode.index < position.episode.total - 1 ? [position.series.index, position.episode.index + 1] :
				position.series.index < position.series.total - 1 ? [position.series.index + 1, 0] : [];
		return [
			this.state.seriesQueue?.[prevEpisodeIndex[0]]?.[prevEpisodeIndex[1]],
			this.state.seriesQueue?.[nextEpisodeIndex[0]]?.[nextEpisodeIndex[1]]
		];
	}

	endscreenCanvasRef = React.createRef();

	render() {
		let props = { ...this.props };
		props.ref = this.videoWrapper;
		for (let prop of ["src", "name", "downloadedItem"])
			delete props[prop];
		props.children = React.Children.toArray(props.children);
		if (this.state.displayFinishScreenEntries) {
			const nextEpisode = this.getAdjacentDownloadedItems()[1];
			if (Consts.AUTO_PLAY && nextEpisode)
				props.children.push((
					<div className={styles.endScreenContainer}>
						<canvas alt="" ref={this.endscreenCanvasRef} />
						<CountdownToNextEpisode nextEpisode={nextEpisode} />
					</div>
				));
			else
				props.children.push((
					<div className={styles.endScreenContainer}>
						<canvas alt="" ref={this.endscreenCanvasRef} />
						<DisplayDownloadedAnime style={{ overflowY: "hidden", opacity: 0.9, "--grid-gap": '2rem' }} noDeleteButton={true} disableDoubleClick={true} downloadedItems={this.state.seriesQueue.flat().filter(ele => !ele.seenThisEpisode())} />
					</div>
				));
		}
		let element = this.props.as ? React.createElement(this.props.as, { ...props }) : <div {...props} />;
		return element;
	}

	handleSrcChange() {
		let video = this.videoWrapper.current.querySelector("video");
		new CacheLocalStorage("videoLastTime").setItem(this.videoHandler.currentName, { currentTime: video.currentTime, progress: video.currentTime / video.duration });
		if (this.subtitlesOctopus)
			try {
				this.subtitlesOctopus.dispose();
			} catch (e) {
			}
			finally {
				clearInterval(this.subtitlesOctopus.resizeInterval);
				delete this.subtitlesOctopus;
			}
		if (this.videoHandler) {
			document.body.removeEventListener("keydown", this.videoHandler.handleKeyDown);
			this.videoHandler.destroy();
		}
		delete this.videoHandler;
		this.setupVideo();
		this.setState({ displayFinishScreenEntries: false });
		this.renderEpisodeQueueStuff(...this.getAdjacentDownloadedItems());
	}

	refreshSeriesQueue(prevDownloadedItem) {
		let currentEpisodeExistsInPlaylist = this.state.seriesQueue.some(
			series => series.some(
				episode => compareAnimeName(episode.episodeData.name, this.props.downloadedItem.episodeData.name)
			)
		);
		if (!currentEpisodeExistsInPlaylist) {
			const prevIndex = this.state.seriesQueue.findIndex(ele => ele.some(ele => compareAnimeName(ele.episodeData.name, prevDownloadedItem.episodeData.name)));
			this.state.seriesQueue.splice(prevIndex + 1, 0, [this.props.downloadedItem]);
		}
	}

	componentDidUpdate(props) {
		this.refreshSeriesQueue(props.downloadedItem);
		if (props.src !== this.props.src)
			this.handleSrcChange();

		this.loadEndScreenBlurredImage();
		this.getIndexInSeriesQueue();
		this.renderEpisodeQueueStuff(...this.getAdjacentDownloadedItems());
	}

	async loadEndScreenBlurredImage() {
		if (this.endscreenCanvasRef.current) {
			let canvasWithData = await renderVideo({
				videoUrl: this.props.src,
				renderedHeight: this.videoWrapper.current?.offsetHeight,
				renderedWidth: this.videoHandler.current?.offsetWidth,
				snapshotTime: 1
			});
			if (this.endscreenCanvasRef.current) // Because await can take a while and meanwhile user might already pass on and this ref will be dead
				this.endscreenCanvasRef.current.getContext("bitmaprenderer").transferFromImageBitmap(canvasWithData.transferToImageBitmap());
		}
	}

	renderEpisodeQueueStuff(prevEpisode, nextEpisode) {
		const container = this.videoWrapper.current;
		if (!container) return;
		if (nextEpisode)
			ReactDom.render(<NextEpisodeButton thumbnailMarginLeft={prevEpisode ? -60 : -25}
				onClick={() => nextEpisode.startPlaying()}
				videoContainer={container} title={nextEpisode.episodeData.name} downloadedItem={nextEpisode} />,
				container.querySelector("#guydhtNextEpisodeButton"));
		if (prevEpisode)
			ReactDom.render(<PrevEpisodeButton thumbnailMarginLeft={10}
				onClick={() => prevEpisode.startPlaying()}
				videoContainer={container} title={prevEpisode.episodeData.name} downloadedItem={prevEpisode} />,
				container.querySelector("#guydhtPrevEpisodeButton"));
		ReactDom.render(<Playlist setSeriesQueue={seriesQueue => this.setState({ seriesQueue })}
			seriesQueue={this.state.seriesQueue}
			currentPosition={this.indexInSeriesQueue} />,
			container.querySelector("#guydhtVideoQueueContainer"));
	}

}

import React, { Component } from "react";
import DownloadedItem from "../../classes/DownloadedItem";
import { DisplayForumEntry } from "../../pages/animeInfo/Forum";
import MALUtils, { ForumEntry } from "../../utils/MAL";
import { loadingComponent } from "../LoadingComponent";

export default loadingComponent(class Comments extends Component<{
	forumEntry?: ForumEntry,
	episode: DownloadedItem
}> {

	render() {
		if (!this.props.forumEntry)
			return (
				<div>
					Couldn't find MAL's forum discussion for {this.props.episode.episodeData.name}
				</div>
			)
		return <DisplayForumEntry forumEntry={this.props.forumEntry} />
	}
}, async ({episode}) => {
	episode.animeEntry.syncGet();
	if(!episode.animeEntry.malId)
		await episode.searchInMal()
	const forumEntry = await MALUtils.forumOfEpisode(episode);
	return { forumEntry };
});

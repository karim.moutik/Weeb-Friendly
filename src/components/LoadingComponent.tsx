import React from "react";
import Modal from "react-bootstrap/Modal";
import Spinner from "react-bootstrap/Spinner";
import styles from "../css/pages/Forum.module.css";

export function loadingComponent<T, K extends {}>(component: React.ComponentType<T & Partial<K>>, componentDidMount: (props: T) => Promise<K>) {
	return class LoadingComponent extends React.Component<T>{

		state: { loading: boolean, results?: K; } = {
			loading: true
		};

		componentDidMount() {
			componentDidMount(this.props).then((results) => this.setState({ loading: false, results }));
		}

		render() {
			if (this.state.loading)
				return loadingComponent.loadingElement;
			if (!this.state.results || !Object.keys(this.state.results).length)
				return (
					<div className={styles.loadingContainer}>
						<Modal.Dialog>
							<Modal.Header>
								Error
							</Modal.Header>
							<Modal.Body>
								Looks like I hit an error :( <br />
								Try again later
								<small>(if you happen to speak to guydht, tell him that {component.name} got undefined as a prop)</small>
							</Modal.Body>
						</Modal.Dialog>
					</div>
				);
			return React.createElement(component, { ...this.props, ...this.state.results });
		}
	};
}

loadingComponent.loadingElement = (
	<div className={styles.loadingContainer}>
		<Modal.Dialog>
			<Modal.Header>
				Loading....
	</Modal.Header>
			<Modal.Body>
				<Spinner animation="border" />
			</Modal.Body>
		</Modal.Dialog>
	</div>
);
